﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Racing
{
    public enum SoundType
    {
        Impact = 10,
        Bonus1 = 20,
        Bonus2 = 30,
        ButtonClick = 40,
    }

    [System.Serializable]
    public class SoundInfo
    {
        public SoundType soundType;
        public AudioClip clip;
    }
    
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager instance = null;
        
        [SerializeField] List<SoundInfo> soundInfos = new List<SoundInfo>();
        [SerializeField] private AudioSource source = null;
        
        private void Awake()
        {
            instance = this;
        }

        public void Play(SoundType soundType)
        {
            var sound = soundInfos.Find(x => x.soundType == soundType);
            if (sound != null)
            {
                source.PlayOneShot(sound.clip);
            }
        }
    }
}
