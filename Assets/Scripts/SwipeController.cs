﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Racing
{
    public class SwipeController : MonoBehaviour
    {
        public static Action<float, float> OnSwipe = delegate { };
        public static Action<float, float> OnStart = delegate { };
        public static Action<float, float> OnRelease = delegate { };

        float currentY;
        float currentX;

        bool isPrevPressed = false;

        void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.currentSelectedGameObject != null)
            {
                isPrevPressed = true;
                return;
            }

            if (isPrevPressed)
            {
                isPrevPressed = false;
                currentX = Input.mousePosition.x;
                currentY = Input.mousePosition.y;
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {   
                currentX = Input.mousePosition.x;
                currentY = Input.mousePosition.y;
                OnStart(currentX, currentY);
            }

            if (Input.GetMouseButtonUp(0))
            {
                OnRelease(currentX, currentY);
            }

            if (Input.GetMouseButton(0))
            {
                float x = Input.mousePosition.x - currentX;
                float y = Input.mousePosition.y - currentY;
                OnSwipe(x, y);
                currentX = Input.mousePosition.x;
                currentY = Input.mousePosition.y;
            }
        }
    }
}
