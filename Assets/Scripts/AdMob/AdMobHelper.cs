﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;
using Racing.UI;

namespace Racing.AdMob
{
    public enum RewardType
    {
        Car = 0,
        Map = 10,
    }
    
    public class AdMobHelper : MonoBehaviour
    {
        public static AdMobHelper instance;
        
        [SerializeField] private string appId;
        [SerializeField] private string rewardedId;
        [SerializeField] private string interstitialId;
        
        private RewardedAd rewardedAd = null;
        private InterstitialAd interstitialAd = null;

        private RewardType currentType = RewardType.Car;
        private int rewardId;
        private bool isInited = false;
        private bool isRewardShown = false;
        private bool isInterstitialShown = false;

        private void Awake()
        {
            instance = this;
        }
        
        private void Start()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
                return;
#if UNITY_EDITOR
            return;
#endif
            isInited = true;

            MobileAds.Initialize(appId);
            CreateAndLoadRewardedAd();
            CreateAndLoadInterstitialdAd();
        }

        private void OnDestroy()
        {
#if UNITY_EDITOR
            return;
#endif

            rewardedAd.OnAdLoaded -= OnRewardLoaded;
            rewardedAd.OnAdFailedToLoad -= OnRewardFailedLoad;
            rewardedAd.OnAdFailedToShow -= OnRewardFailedShow;
            rewardedAd.OnUserEarnedReward -= OnRewardShown;
            rewardedAd.OnAdOpening -= OnRewardOpening;
            rewardedAd.OnAdClosed -= OnRewardClosed;
            
            interstitialAd.OnAdLoaded -= OnInterstitialLoaded;
            interstitialAd.OnAdFailedToLoad -= OnInterstitialFailedLoad;
            interstitialAd.OnAdOpening -= OnInterstitialOpening;          
            interstitialAd.Destroy();
        }

        private void CreateAndLoadInterstitialdAd()
        {
            interstitialAd = new InterstitialAd(interstitialId);
            AdRequest requestInter = new AdRequest.Builder().Build();
            interstitialAd.LoadAd(requestInter);
            interstitialAd.OnAdLoaded += OnInterstitialLoaded;
            interstitialAd.OnAdFailedToLoad += OnInterstitialFailedLoad;
            interstitialAd.OnAdOpening += OnInterstitialOpening;
            interstitialAd.OnAdClosed += OnInterstitialClosed;
        }
        
        private void CreateAndLoadRewardedAd()
        {
            rewardedAd = new RewardedAd(rewardedId);

            rewardedAd.OnAdLoaded += OnRewardLoaded;
            rewardedAd.OnAdFailedToLoad += OnRewardFailedLoad;
            rewardedAd.OnAdFailedToShow += OnRewardFailedShow;
            rewardedAd.OnUserEarnedReward += OnRewardShown;
            rewardedAd.OnAdOpening += OnRewardOpening;
            rewardedAd.OnAdClosed += OnRewardClosed;

            AdRequest request = new AdRequest.Builder().Build();
            rewardedAd.LoadAd(request);
        }
        
        public void ShowRewardedAd(RewardType rewardType, int id)
        {
#if UNITY_EDITOR
            GameUI.instance.OnShowRewardComplete(rewardType, id);
#endif
            if (!isInited)
                return;

            Debug.Log("!!! Try show RewardedAd");
            if (rewardedAd.IsLoaded())
            {
                currentType = rewardType;
                rewardId = id;
                rewardedAd.Show();
                Debug.Log("!!! Show RewardedAd start");
            }
            else
            {
                Debug.Log("!!! RewardedAd not loaded");
            }
            
            AdRequest request = new AdRequest.Builder().Build();
            rewardedAd.LoadAd(request);
        }

        public void ShowInterstitialAd()
        {
            if (!isInited)
                return;
            
            if (interstitialAd.IsLoaded())
            {
                Time.timeScale = 0f;
                interstitialAd.Show();
            }
            else
            {
                Debug.Log("!!! InterstitialAd not loaded");
            }
        }

        private void Update()
        {
            if (isRewardShown)
            {
                isRewardShown = false;
                GameUI.instance.OnShowRewardComplete(currentType, rewardId);
                return;
            }

            if (isInterstitialShown)
            {
                isInterstitialShown = false;
                Time.timeScale = 1f;
                return;
            }
        }

        private void OnInterstitialClosed(object obj, EventArgs args)
        {
            CreateAndLoadInterstitialdAd();
            isInterstitialShown = true;
        }

        private void OnRewardClosed(object obj, EventArgs args)
        {
            CreateAndLoadRewardedAd();
        }

        private void OnRewardShown(object obj, Reward reward)
        {
            Debug.Log("!!! OnRewardShown");
            isRewardShown = true;
        }
        
        private void OnRewardLoaded(object obj, EventArgs args)
        {
            Debug.Log("!!! OnRewardLoaded");
        }

        private void OnRewardFailedLoad(object obj, AdErrorEventArgs args)
        {
            Debug.Log("!!! OnRewardFailedLoad: " + args.Message);
        }

        private void OnRewardFailedShow(object obj, EventArgs args)
        {
            Debug.Log("!!! OnRewardFailedShow");
        }

        private void OnRewardOpening(object obj, EventArgs args)
        {
            Debug.Log("!!! OnRewardOpening");
        }

        private void OnInterstitialLoaded(object obj, EventArgs args)
        {
            Debug.Log("!!! OnInterstitialLoaded");
            Time.timeScale = 1f;
        }

        private void OnInterstitialFailedLoad(object obj, AdFailedToLoadEventArgs args)
        {
            Debug.Log("!!! OnInterstitialFailedLoad: " + args.Message);
        }

        private void OnInterstitialOpening(object obj, EventArgs args)
        {
            Debug.Log("!!! OnInterstitialOpening");
        }
    }
}
