﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Racing.Utils
{
    public class PoolManager : MonoBehaviour
    {
        public static PoolManager instance;

        private readonly IDictionary<string, List<GameObject>> storage = new Dictionary<string, List<GameObject>>();
        private readonly IDictionary<GameObject, string> itemsInUse = new Dictionary<GameObject, string>();
        private readonly IDictionary<string, PoolConfig> poolConfigsByName = new Dictionary<string, PoolConfig>();
        private readonly IDictionary<string, PoolFillRequest> fillRequests = new Dictionary<string, PoolFillRequest>();

        private void Awake()
        {
            instance = this;
        }

        public void Clear()
        {
            itemsInUse.Clear();
            fillRequests.Clear();

            foreach(var it in storage)
            {
                for (int i = 0; i < it.Value.Count; ++i)
                {
                    Destroy(it.Value[i]);
                    it.Value.Clear();
                }
            }
            storage.Clear();
            foreach (var it in poolConfigsByName)
            {
                Destroy(it.Value.Wrapper);
            }
            poolConfigsByName.Clear();
        }

        public void InitPoolingPrefab(GameObject prefab, int initialCount = 10)
        {
            if (!storage.ContainsKey(prefab.name))
            {
                if (prefab.name.Contains("(Clone)"))
                    throw new Exception("Prefab based objects are only allowed");

                PoolConfig config = new PoolConfig
                {
                    Prefab = prefab,
                    InitialCount = initialCount
                };
                
                InitCustomConfig(config);
            }
        }

        public T GetFromPool<T>(T prefabComponent) where T : Component
        {
            return GetFromPool(prefabComponent.gameObject, Vector3.zero, Quaternion.identity).GetComponent<T>();
        }

        public T GetFromPool<T>(T prefabComponent, Vector3 position, Quaternion rotation) where T : Component
        {
            return GetFromPool(prefabComponent.gameObject, position, rotation).GetComponent<T>();
        }

        public GameObject GetFromPool(GameObject prefab)
        {
            return GetFromPool(prefab, Vector3.zero, Quaternion.identity);
        }

        public GameObject GetFromPool(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            InitPoolingPrefab(prefab);
            return GetFromPoolNotInitable(prefab.name, position, rotation);
        }

        public bool ReturnToPool(Component objComponent)
        {
            return ReturnToPool(objComponent.gameObject);
        }

        public bool ReturnToPool(GameObject obj)
        {
            if (obj != null && itemsInUse.ContainsKey(obj))
            {
                obj.SetActive(false);
                string poolName = itemsInUse[obj];
                PoolConfig config = poolConfigsByName[poolName];
                config.ItemsInUse -= 1;
                itemsInUse.Remove(obj);
                storage[poolName].Add(obj);
                obj.transform.parent = config.Wrapper.transform;
                obj.transform.position = Vector3.zero;
                return true;
            }
            
            if (obj != null && obj.transform.IsChildOf(transform))
                return true;
            
            return false;
        }

        public GameObject PrefabOf(GameObject obj)
        {
            if (obj != null && itemsInUse.ContainsKey(obj))
            {
                string poolName = itemsInUse[obj];
                PoolConfig config = poolConfigsByName[poolName];
                return config.Prefab;
            }

            return null;
        }

        private GameObject GetFromPoolNotInitable(string poolName, Vector3 position, Quaternion rotation)
        {
            if (storage.ContainsKey(poolName))
            {
                List<GameObject> vacantObjects = storage[poolName];
                PoolConfig config = poolConfigsByName[poolName];
                
                if (vacantObjects.Count == 0)
                {
                    PoolFillRequest request = new PoolFillRequest
                    {
                        Config = config,
                        VacantObjects = vacantObjects,
                        ToFillCount = 1
                    };
                    CompleteRequest(request);
                }

                if (vacantObjects.Count > 0)
                {
                    GameObject vacantObject = vacantObjects[vacantObjects.Count - 1];
                    vacantObject.transform.parent = null;
                    vacantObject.transform.position = position;
                    vacantObject.transform.rotation = rotation;
                    itemsInUse.Add(vacantObject, poolName);                    
                    vacantObjects.Remove(vacantObject);
                    
                    if (vacantObjects.Count == 0)
                    {
                        if (!fillRequests.ContainsKey(poolName))
                        {
                            PoolFillRequest request = new PoolFillRequest
                            {
                                Config = config,
                                VacantObjects = vacantObjects,
                                ToFillCount = 3
                            };
                            fillRequests.Add(poolName, request);
                        }
                    }
                    
                    config.ItemsInUse += 1;
                    vacantObject.SetActive(true);
                    return vacantObject;
                }
            }
            
            throw new Exception("No such pool named " + poolName);
        }
        
        private void InitCustomConfig(PoolConfig config)
        {
            string poolName = config.Prefab.name;
            GameObject wrapper = new GameObject(poolName + "_Pool");
            config.Wrapper = wrapper;
            wrapper.transform.position = Vector3.zero;
            wrapper.transform.parent = transform;
            List<GameObject> list = new List<GameObject>(config.InitialCount);
            storage.Add(poolName, list);
            for (int i = 0; i < config.InitialCount; i++)
            {
                var o = InitObject(config);
                list.Add(o);
            }

            poolConfigsByName.Add(poolName, config);
        }
        
        private GameObject InitObject(PoolConfig config)
        {
            GameObject o = Instantiate(config.Prefab);
            o.transform.position = Vector3.zero;
            o.transform.parent = config.Wrapper.transform;
            o.SetActive(false);
            return o;
        }
        
        private int CompleteRequest(PoolFillRequest request)
        {
            List<GameObject> vacantObjects = request.VacantObjects;
            PoolConfig config = request.Config;

            if (config.ItemsInUse < config.MaximumCount)
            {
                int newCount = Mathf.Min(config.MaximumCount, vacantObjects.Count + config.ItemsInUse + 1);
                int spawnedCount = 0;
                for (int i = 0; i < newCount - (config.ItemsInUse + vacantObjects.Count); i++)
                {
                    GameObject o = InitObject(config);
                    vacantObjects.Add(o);
                    spawnedCount++;
                }
                return spawnedCount;
            }
            
            Debug.LogWarning(String.Format("PoolManager: pool, named {0} exceed maximum object count {1}",
                config.Prefab.name, config.MaximumCount));
            return 0;
        }
        
        [Serializable]
        private class PoolConfig
        {
            public GameObject Prefab;
            public int InitialCount = 2;
            public int MaximumCount = 100;
            private GameObject wrapper;
            private int itemsInUse;

            public GameObject Wrapper
            {
                get { return wrapper; }
                set { wrapper = value; }
            }

            public int ItemsInUse
            {
                get { return itemsInUse; }
                set { itemsInUse = value; }
            }
        }
        
        private class PoolFillRequest
        {
            public List<GameObject> VacantObjects;
            public PoolConfig Config;
            public int ToFillCount;
        }
    }
}
