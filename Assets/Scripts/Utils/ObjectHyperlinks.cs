﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Racing.Utils
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ObjectHyperlinks : MonoBehaviour, IPointerClickHandler
    {
        private UnityEngine.Camera cam = null;
        private TextMeshProUGUI textMeshPro = null;

        private void Awake()
        {
            cam = UnityEngine.Camera.main;
            textMeshPro = GetComponent<TextMeshProUGUI>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(textMeshPro, cam.WorldToScreenPoint(Input.mousePosition), cam);
            if (linkIndex != -1)
            {
                TMP_LinkInfo linkInfo = textMeshPro.textInfo.linkInfo[linkIndex];
                Application.OpenURL(linkInfo.GetLinkID());
            }
        }
    }
}
