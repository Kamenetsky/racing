﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

namespace Racing.Utils
{
	public class GameSoundController : MonoBehaviour 
	{
		public static GameSoundController instance;

		[System.Serializable]
		public class SoundType
		{
			public string name;
			public AudioClip sound;
		}

		[SerializeField]
		AudioSource music = null;

		[SerializeField]
		AudioSource oneShotSource = null;

		[SerializeField]
		AudioSource[] loopedSources = null;

		[SerializeField]
		List<SoundType> sounds = null;

		void Awake()
		{
			instance = this;
		}

		void Start () 
		{
			if (music != null)
				music.Play ();
		}
		
		public void PlaySound(string name, float val = 1f)
		{
			var sound = sounds.Find (x => x.name == name);
			if (sound != null)
				oneShotSource.PlayOneShot (sound.sound, val);
		}

		public void PlayLooped(string name, float val = 1f)
		{
			var sound = sounds.Find (x => x.name == name);
			if (sound != null) 
			{
				if (System.Array.Exists (loopedSources, x => x.clip == sound.sound && x.isPlaying))
					return;
				
				foreach (var it in loopedSources) 
				{
					if (it.clip == null || !it.isPlaying) 
					{
						it.volume = val;
						it.clip = sound.sound;
						it.Play ();
						return;
					}
				}
			}
		}

		public void StopAllLooped()
		{
			foreach (var it in loopedSources)
				it.Stop ();
		}

		public void StopLooped(string name)
		{
			var sound = sounds.Find (x => x.name == name);
			if (sound != null) 
			{
				foreach (var it in loopedSources) 
				{
					if (it.clip == sound.sound) 
					{
						it.Stop ();
						return;
					}
				}
			}
		}
	}
}
