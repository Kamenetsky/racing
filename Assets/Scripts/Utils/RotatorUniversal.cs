﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Racing.Utils
{
    public class RotatorUniversal : MonoBehaviour
    {
        [SerializeField] private float speed = 10f;
        [SerializeField] private Vector3 axis = Vector3.up;
        
        private void Update()
        {
            transform.RotateAround(transform.position, axis, Time.deltaTime * speed);
        }
    }
}
