﻿using System;
using System.Collections.Generic;
using Racing;
using UnityEngine;

namespace Racing.Utils
{
    public class Bezier
    {
        public static Checkpoint GetCheckpoint(List<Checkpoint> checkpoints, float t)
        {
            List<Checkpoint> extendedCpts = new List<Checkpoint>(checkpoints);
            extendedCpts.Add(extendedCpts[0]); //HACK: cycled
            extendedCpts.Add(extendedCpts[1]);
            extendedCpts.Add(extendedCpts[2]);
            int numSect = extendedCpts.Count - 3;
            int currSect = Mathf.Min(Mathf.FloorToInt(t * numSect), numSect - 1);
            float u = t * numSect - currSect;
            Vector3 a = extendedCpts[currSect].position;
            Vector3 b = extendedCpts[currSect + 1].position;
            Vector3 c = extendedCpts[currSect + 2].position;
            Vector3 d = extendedCpts[currSect + 3].position;
            Checkpoint result = new Checkpoint();
            result.position = .5f * (
                                  (-a + 3f * b - 3f * c + d) * (u * u * u)
                                  + (2f * a - 5f * b + 4f * c - d) * (u * u)
                                  + (-a + c) * u
                                  + 2f * b
                              );

            float dist = Mathf.Infinity;
            int index = 0;
            for (int i = 0; i < 4; ++i)
            {
                float newDist = Vector3.Distance(result.position, extendedCpts[currSect + i].position);
                if (newDist < dist)
                {
                    index = i;
                    dist = newDist;
                }
            }

            float dist1 = Mathf.Infinity;
            int index1 = 0;
            for (int i = 0; i < 4; ++i)
            {
                if (i == index)
                    continue;

                float newDist = Vector3.Distance(result.position, extendedCpts[currSect + i].position);
                if (newDist < dist1)
                {
                    index1 = i;
                    dist1 = newDist;
                }
            }

            result.normal = Vector3.Lerp(extendedCpts[currSect + index].normal, extendedCpts[currSect + index1].normal,
                dist / Vector3.Distance(extendedCpts[currSect + index].position,
                    extendedCpts[currSect + index1].position)).normalized;

            return result;
        }

        public static Vector3 GetPoint3(Vector3[] pts, float t)
        {
            t = Mathf.Clamp01(t);
            float u = 1f - t;

            Vector3 a = pts[0];
            Vector3 b = pts[1];
            Vector3 c = pts[2];

            return u * u * a + 2f * u * t * b + t * t * c;
        }

        public static Vector3 GetPoint4(Vector3[] pts, float t)
        {
            int numSections = pts.Length - 3;
            int currPt = Mathf.Min(Mathf.FloorToInt(t * (float) numSections), numSections - 1);
            float u = t * (float) numSections - (float) currPt;

            Vector3 a = pts[currPt];
            Vector3 b = pts[currPt + 1];
            Vector3 c = pts[currPt + 2];
            Vector3 d = pts[currPt + 3];

            return .5f * (
                       (-a + 3f * b - 3f * c + d) * (u * u * u)
                       + (2f * a - 5f * b + 4f * c - d) * (u * u)
                       + (-a + c) * u
                       + 2f * b
                   );
        }

        public static Vector3[] PathControlPoints(Vector3[] path, int offset = 2)
        {
            Vector3[] suppliedPath;
            Vector3[] vector3s;

            suppliedPath = path;

            vector3s = new Vector3[suppliedPath.Length + offset];
            Array.Copy(suppliedPath, 0, vector3s, 1, suppliedPath.Length);

            vector3s[0] = vector3s[1] + (vector3s[1] - vector3s[2]);
            vector3s[vector3s.Length - 1] = vector3s[vector3s.Length - 2] +
                                            (vector3s[vector3s.Length - 2] - vector3s[vector3s.Length - 3]);

            if (vector3s[1] == vector3s[vector3s.Length - 2])
            {
                Vector3[] tmpLoopSpline = new Vector3[vector3s.Length];
                Array.Copy(vector3s, tmpLoopSpline, vector3s.Length);
                tmpLoopSpline[0] = tmpLoopSpline[tmpLoopSpline.Length - 3];
                tmpLoopSpline[tmpLoopSpline.Length - 1] = tmpLoopSpline[2];
                vector3s = new Vector3[tmpLoopSpline.Length];
                Array.Copy(tmpLoopSpline, vector3s, tmpLoopSpline.Length);
            }

            return (vector3s);
        }
    }
}
