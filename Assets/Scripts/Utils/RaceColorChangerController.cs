﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using DG.Tweening;
using UnityEngine;

namespace Racing.Utils
{
    public class RaceColorChangerController : MonoBehaviour
    {
        [SerializeField] private RaceColorChangerData data = null;

        private Dictionary<GameObject, List<MaterialParamInfoCommon>> matByObjects = new Dictionary<GameObject, List<MaterialParamInfoCommon>>();
        
        private Color currColor = Color.white;
        private Color nextColor = Color.white;

        public void AddGameObject(GameObject go)
        {
            if (data == null)
                return;
            
            var rends = go.GetComponentsInChildren<Renderer>();
            if (rends == null || rends.Length == 0)
                return;

            foreach (var rend in rends)
            {
                var mats = rend.materials;
                foreach (var mat in mats)
                {
                    string matName = mat.name.Replace(" (Instance)", "");
                    var propMat = data.Materials.Find(x => x.materialName == matName);
                    if (propMat == null)
                        continue;
                
                    if (mat.HasProperty(propMat.paramName))
                    {
                        if (!matByObjects.ContainsKey(go))
                        {
                            matByObjects.Add(go,
                                new List<MaterialParamInfoCommon>()
                                    {new MaterialParamInfoCommon() {material = mat, paramName = propMat.paramName}});
                        }
                        else
                        {
                            matByObjects[go].Add(new MaterialParamInfoCommon() {material = mat, paramName = propMat.paramName});
                        }

                        go.GetStateListener().OnDestroied += delegate { RemoveGameObject(go); };
                    }
                }  
            }
        }

        private void RemoveGameObject(GameObject go)
        {
            if (matByObjects.ContainsKey(go))
            {
                matByObjects.Remove(go);
            }
        }

        private void Start()
        {
            if (data == null || data.Colors.Length < 2 || data.ChangeTime <= 0f)
                return;


            ChangeColor();
        }

        private void ChangeColor()
        {
            Color nextColor = data.Colors[Random.Range(0, data.Colors.Length)];
            foreach (var it in matByObjects)
            {
                foreach (var matInfo in it.Value)
                {
                    matInfo.material.DOColor(nextColor, matInfo.paramName, data.ChangeTime).OnComplete(() =>
                    {
                        StopChanging();
                        ChangeColor();
                    });
                }
            }
        }

        private void StopChanging()
        {
            foreach (var it in matByObjects)
            {
                foreach (var matInfo in it.Value)
                {
                    matInfo.material.DOKill();
                }
            }
        }
    }
}
