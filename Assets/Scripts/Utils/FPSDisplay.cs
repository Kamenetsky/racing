﻿using UnityEngine;
using UnityEngine.UI;

namespace Racing.Utils
{
    public class FPSDisplay : MonoBehaviour
    {
        private int avgFrameRate;
        [SerializeField] Text text = null;

        public void Update()
        {
            float current = 0;
            current = (int) (1f / Time.unscaledDeltaTime);
            avgFrameRate = (int) current;
            text.text = avgFrameRate.ToString() + " FPS";
        }
    }
}