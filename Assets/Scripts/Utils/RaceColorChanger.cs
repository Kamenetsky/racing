﻿using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Racing.Utils
{
    public class RaceColorChanger : MonoBehaviour
    {
        [SerializeField] private RaceColorChangerData data = null;

        private Dictionary<MaterialParamInfo, Material> matByParam = new Dictionary<MaterialParamInfo, Material>();
        private bool hasParameters = false;

        private void Awake()
        {
            if (data == null)
                return;
            
            var mats = GetComponent<Renderer>().materials;

            foreach (var mat in mats)
            {
                string matName = mat.name.Replace(" (Instance)", "");
                var propMat = data.Materials.Find(x => x.materialName == matName);
                if (propMat == null)
                    continue;
                
                if (mat.HasProperty(propMat.paramName))
                {
                    hasParameters = true;
                    if (!matByParam.ContainsKey(propMat))
                        matByParam.Add(propMat, mat);
                }
            }  
        }

        private void Start()
        {
            if (!hasParameters)
                return;

            if (data == null || data.Colors.Length < 2 || data.ChangeTime <= 0f)
                return;


            ChangeColor();
        }

        private void ChangeColor()
        {
            Color nextColor = data.Colors[Random.Range(0, data.Colors.Length)];
            foreach (var it in matByParam)
            {
                it.Value.DOColor(nextColor, it.Key.paramName, data.ChangeTime).OnComplete(() =>
                {
                    StopChanging();
                    ChangeColor();
                });
            }
        }

        private void StopChanging()
        {
            foreach (var it in matByParam)
            {
                it.Value.DOKill();
            }
        }
    }
}
