﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Racing.Utils
{
    [System.Serializable]
    public class MaterialParamInfo
    {
        public string materialName;
        public string paramName;
    }
    
    public class MaterialParamInfoCommon
    {
        public Material material;
        public string paramName;
    }
    
    [CreateAssetMenu(fileName = "RaceColorChangerData", menuName = "Roads/ColorChangerData", order = 1)]
    public class RaceColorChangerData : ScriptableObject
    {
        [SerializeField] private float changeTime = 1f;
        [SerializeField] private Color[] colors = null;
        [SerializeField] private List<MaterialParamInfo> materials = new List<MaterialParamInfo>();

        public float ChangeTime => changeTime;
        public Color[] Colors => colors;
        public List<MaterialParamInfo> Materials => materials;
    }
}
