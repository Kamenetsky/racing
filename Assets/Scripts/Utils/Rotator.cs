﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Racing.Utils
{
    public class Rotator : MonoBehaviour
    {
        private float speed;
        private const float maxSpeed = 50;
        
        private void Start()
        {
            speed = Random.Range(-maxSpeed, maxSpeed);
        }

        private void Update()
        {
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime * speed);
        }
    }
}
