﻿using System;
using UnityEngine;

namespace Racing.Utils
{
    public static class ObjectStateExtensions
    {
        public static IStateListener GetStateListener(this GameObject obj)
        {
            return obj.GetComponent<ObjectStateListener>() ?? obj.AddComponent<ObjectStateListener>();
        }

        public interface IStateListener
        {
            event Action OnEnabled;
            event Action OnDisabled;
            event Action OnDestroied;

            void Dispose();
        }

        private class ObjectStateListener : MonoBehaviour, IStateListener
        {
            public event Action OnEnabled;
            public event Action OnDisabled;
            public event Action OnDestroied;

            public void Dispose()
            {
                OnEnabled = null;
                OnDisabled = null;
                OnDestroied = null;
            }

            private void Awake()
            {
                hideFlags = HideFlags.DontSaveInBuild | HideFlags.HideInInspector;
            }

            private void OnEnable()
            {
                TryInvoke(OnEnabled);
            }

            private void OnDisable()
            {
                TryInvoke(OnDisabled);
            }

            private void OnDestroy()
            {
                TryInvoke(OnDestroied);
            }

            private void TryInvoke(Action action)
            {
                if (action != null)
                    action();
            }
        }
    }
}