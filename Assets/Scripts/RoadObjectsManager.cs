﻿using System.Collections;
using System.Collections.Generic;
using Racing.Utils;
using UnityEngine;

namespace Racing
{
    public class RoadObjectsManager : MonoBehaviour
    {
        [SerializeField] private GameObject bonusPrefab = null;
        [SerializeField] private GameObject obstaclePrefab = null;
        [SerializeField] private GameObject speedPrefab = null;
        [SerializeField] private GameObject healthPrefab = null;
        [SerializeField] private float addBonusProbability = 0.2f;
        
        private Dictionary<Checkpoint, GameObject> pointsByObjects = new Dictionary<Checkpoint, GameObject>();
        private List<List<Checkpoint>> checkpoints = new List<List<Checkpoint>>();
        private LevelInfo currentLevel = null;
        private int currCheckpoint = 0;
        
        private const int maxBonuses = 5;
        private const float bonusTimeInterval = 1.5f;
        private const float bonusTimeJitter = 0.2f;
        private int minBonusIdShift = 30;
        private int maxBonusIdShift = 50;
        private float bonusTime = 0f;
        
        private const float obstacleTimeInterval = 0.8f;
        private const float obstacleTimeJitter = 0.1f;
        private int minObstacleIdShift = 40;
        private int maxObstacleIdShift = 60;
        private float obstacleTime = 0f;

        private int maxObjectsCount = 50;
        private const float upObjectsShift = 0.8f;

        private const float startDelay = 3f;
        private float startTime = 0f;
        private bool isInited = false;
        private bool isStarted = false;
        
        private UnityEngine.Camera cam = null;

        public void Init(LevelInfo level)
        {
            currentLevel = level;
            checkpoints.Clear();
            checkpoints.Add(currentLevel.CheckpointsLeft);
            checkpoints.Add(currentLevel.CheckpointsCenter);
            checkpoints.Add(currentLevel.CheckpointsRight);
            startTime = 0f;
            PoolManager.instance.Clear();
            PoolManager.instance.InitPoolingPrefab(obstaclePrefab, maxObjectsCount);
            PoolManager.instance.InitPoolingPrefab(bonusPrefab, 10);
            PoolManager.instance.InitPoolingPrefab(speedPrefab, 2);
            PoolManager.instance.InitPoolingPrefab(healthPrefab, 2);
            isInited = true;
            isStarted = false;
        }

        public void UnInit()
        {
            isStarted = false;
            isInited = false;
            pointsByObjects.Clear();
        }

        public void UpdateInfo(int checkpointId)
        {
            if (currCheckpoint != checkpointId)
            {
                currCheckpoint = checkpointId;
                RemoveBackObjects();
            }
        }

        private void Awake()
        {
            cam = UnityEngine.Camera.main;
        }

        private void Update()
        {
            if (isInited && !isStarted)
            {
                startTime += Time.deltaTime;
                if (startTime > startDelay)
                {
                    isStarted = true;
                }
            }
            
            if (!isStarted)
                return;

            bonusTime += Time.deltaTime;
            obstacleTime += Time.deltaTime;
            
            if (bonusTime > bonusTimeInterval)
            {
                bonusTime = Random.Range(-bonusTimeJitter, bonusTimeJitter);
                int amount = Random.Range(1, maxBonuses);
                for (int i = 0; i < amount; ++i)
                    GenerateBonus();
            }

            if (obstacleTime > obstacleTimeInterval)
            {
                obstacleTime = Random.Range(-obstacleTimeJitter, obstacleTimeJitter);
                GenerateObstacle();
            }
        }
        
        private void GenerateBonus()
        {
            if (pointsByObjects.Count > maxObjectsCount)
                return;
            
            if (currCheckpoint > checkpoints[0].Count - 100)
                return;
                      
            int checkpointID = currCheckpoint + Random.Range(minBonusIdShift, maxBonusIdShift);
            var selCheckpoint = checkpoints[Random.Range(0, checkpoints.Count)];
            if (pointsByObjects.ContainsKey(selCheckpoint[checkpointID]))
            {
                GenerateBonus();
                return;
            }

            var prefab = Random.value < addBonusProbability
                ? (Random.value < 0.5f ? speedPrefab : healthPrefab)
                : bonusPrefab;
            var bonus = PoolManager.instance.GetFromPool(prefab);
            var mr = bonus.GetComponent<MeshRenderer>();
            if (mr != null)
                mr.enabled = true;

            bonus.transform.position = selCheckpoint[checkpointID].position + Vector3.up * upObjectsShift;      
            pointsByObjects.Add(selCheckpoint[checkpointID], bonus);
            var targetRotation = Quaternion.FromToRotation(Vector3.up, selCheckpoint[checkpointID].normal);
            Vector3 nextPos = checkpointID > 0 ? selCheckpoint[checkpointID - 1].position :  selCheckpoint[checkpointID + 1].position; 
            var horizontalVector = new Vector3(nextPos.x - selCheckpoint[checkpointID].position.x, 0f, nextPos.z - selCheckpoint[checkpointID].position.z).normalized;
            targetRotation *= Quaternion.LookRotation(horizontalVector);
            bonus.transform.rotation = targetRotation;
            bonus.transform.parent = currentLevel.transform;               
        }

        private void GenerateObstacle()
        {
            if (pointsByObjects.Count > maxObjectsCount)
                return;
            
            if (currCheckpoint > checkpoints[0].Count - 100)
                return;
            
            int checkpointID = currCheckpoint + Random.Range(minObstacleIdShift, maxObstacleIdShift);
            var selCheckpoint = checkpoints[Random.Range(0, checkpoints.Count)];
            if (pointsByObjects.ContainsKey(selCheckpoint[checkpointID]))
            {
                GenerateObstacle();
                return;
            }
            var obstacle = PoolManager.instance.GetFromPool(obstaclePrefab);
            pointsByObjects.Add(selCheckpoint[checkpointID], obstacle);
            obstacle.transform.position = selCheckpoint[checkpointID].position + Vector3.up * upObjectsShift; 
            obstacle.transform.LookAt(obstacle.transform.position + selCheckpoint[checkpointID].normal * 5);
            obstacle.transform.parent = currentLevel.transform;
        }
        
        private void RemoveBackObjects()
        {
            List<Checkpoint> keys = new List<Checkpoint>();
            foreach (var kvp in pointsByObjects)
            {
                var heading = kvp.Key.position - cam.transform.position;
                var dot = Vector3.Dot(heading, cam.transform.forward);

                if (dot < 0f)
                {
                    PoolManager.instance.ReturnToPool(kvp.Value);
                    kvp.Value.GetStateListener().Dispose();
                    keys.Add(kvp.Key);
                }
            }

            foreach (var key in keys)
                pointsByObjects.Remove(key);
        }
    }
}
