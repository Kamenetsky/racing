﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Racing.Camera
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class CameraControllerCheckpointed : MonoBehaviour
    {
        public static CameraControllerCheckpointed instance;

        [Range(0, -100)]
        [SerializeField] private int backOffset = -10;
        [SerializeField] private float upOffset = 5f;
        [SerializeField] private float lookOffset = 2.5f;
        [SerializeField] private float forwardOffset = 10f;
        [SerializeField] private float horizontalRotSpeed = 10f;
        
        private Transform target = null;
        private float carSpeed = 1f;
        private float nitroMultiplier = 1f;
        private bool isNitro = false;

        private List<List<Checkpoint>> lanes = null;
        private int currCheckpoint = 0;
        private int currLaneIndex = 0;

        private UnityEngine.Camera cam = null;
        private float fov = 60f;
        [SerializeField] private float fovOffset = 10;

        private bool isInited = false;
        
        public void Init(Transform target, float carSpeed, float nitroMultiplier, List<List<Checkpoint>> lanes)
        {
            this.target = target;
            this.carSpeed = carSpeed;
            this.nitroMultiplier = nitroMultiplier;
            this.lanes = lanes;
            isInited = true;
        }

        public void UnInit()
        {
            isInited = false;
        }

        public void UpdateInfo(int currCheckpoint, int currLaneIndex)
        {
            this.currCheckpoint = currCheckpoint;
            this.currLaneIndex = currLaneIndex;
        }

        public void SetSpeedOffset(bool isOn)
        {
            cam.DOFieldOfView(isOn ? (fov + fovOffset) : fov, 0.5f);
            isNitro = isOn;
        }
        
        private void Awake()
        {
            instance = this;
            cam = UnityEngine.Camera.main;
            fov = cam.fieldOfView;
        }

        private float zShift;
        
        private void LateUpdate()
        {
            if (!isInited)
                return;
            
            int camCheckpoint = Checkpoint.GetCheckpointByOffset(lanes[currLaneIndex], currCheckpoint, backOffset);
            Vector3 destPos = lanes[currLaneIndex][camCheckpoint].position + Vector3.up * upOffset;
            transform.position = Vector3.MoveTowards(transform.position, destPos,
                Time.deltaTime * (carSpeed * (isNitro ? nitroMultiplier : 1f)));
            var rot = Quaternion.LookRotation(target.position - transform.position + target.forward * forwardOffset + Vector3.up * lookOffset); //look at
            zShift = Mathf.MoveTowardsAngle(zShift, target.parent.localRotation.eulerAngles.z, Time.deltaTime * horizontalRotSpeed);
            rot *= Quaternion.Euler(new Vector3(0, 0, zShift));
            transform.rotation = rot;
        }
    }
}
