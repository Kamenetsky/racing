﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using DG.Tweening;
using UnityEngine;

namespace Racing.Camera
{
    public class CameraControllerParented : MonoBehaviour
    {
        public static CameraControllerParented instance;
        
        [SerializeField] private Vector3 offset = Vector3.one;
        [SerializeField] private Vector3 speedOffset = Vector3.one;
        [SerializeField] private Vector3 angle = Vector3.zero;

        private Vector3 overridedOffset = Vector3.one;
        private bool isOffsetOverrided = false;
      

        private void Awake()
        {
            instance = this;
        }
        
        public void SetParent(Transform prnt)
        {
            transform.SetParent(prnt);
            transform.localPosition = offset;
            transform.localEulerAngles = angle;
            overridedOffset = offset;
        }

        public void Unparent()
        {
            transform.SetParent(null);
        }

        public void OverrideOffset(Vector3 offset, Vector3 angle)
        {
            isOffsetOverrided = true;
            overridedOffset = offset;
            transform.localPosition = offset;
            transform.localEulerAngles = angle;
        }

        public void SetSpeedOffset(bool isOn)
        {
            Vector3 speedOffsetCurremt = new Vector3(0f, speedOffset.y / offset.y * overridedOffset.y, speedOffset.z / offset.z * overridedOffset.z);
            transform.DOLocalMove(isOn ? speedOffsetCurremt : isOffsetOverrided ? overridedOffset : offset, 0.5f);
        }
    }
}
