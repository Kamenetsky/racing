﻿using UnityEngine;

using DG.Tweening;

namespace Racing.Camera
{
    public class CameraControllerTargeted : MonoBehaviour
    {
        public static CameraControllerTargeted instance;

        [SerializeField] private Transform target = null;
        
        [SerializeField] private Vector3 offset = Vector3.one;
        [SerializeField] private Vector3 speedOffset = Vector3.one;
        [SerializeField] private Vector3 angle = Vector3.zero;

        [SerializeField] private float moveSpeed = 1f;
        [SerializeField] private float rotSpeed = 1f;

        private Vector3 overridedOffset = Vector3.one;
        private bool isOffsetOverrided = false;

        private bool isTargeted = false;

        private Transform aim;

        private void Awake()
        {
            instance = this;

            if (target == null)
            {
                Debug.LogError("Can not find camera target!");
            }
        }
        
        public void SetTarget(Transform prnt)
        {
            isTargeted = true;
            aim = prnt;
            target.SetParent(prnt);
            target.localPosition = offset;
            target.localEulerAngles = angle;
            transform.position = target.position + target.up * 500f - target.forward * 500f;
            transform.LookAt(target);
            //transform.rotation = target.rotation;
            overridedOffset = offset;
        }

        public void Untarget()
        {
            isTargeted = false;
            target.SetParent(transform);
        }

        public void OverrideOffset(Vector3 offset, Vector3 angle)
        {
            isOffsetOverrided = true;
            overridedOffset = offset;
            target.localPosition = offset;
            target.localEulerAngles = angle;
        }

        public void SetSpeedOffset(bool isOn)
        {
            Vector3 speedOffsetCurremt = new Vector3(0f, speedOffset.y / offset.y * overridedOffset.y, speedOffset.z / offset.z * overridedOffset.z);
            target.DOLocalMove(isOn ? speedOffsetCurremt : isOffsetOverrided ? overridedOffset : offset, 0.5f);
        }

        private void LateUpdate()
        {
            if (!isTargeted)
                return;

            transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * moveSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, Time.deltaTime * rotSpeed);
        }
    }
}
