﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Racing.Camera
{
    [ExecuteInEditMode]
    public class CameraController : MonoBehaviour
    {
        public static CameraController instance;
        
        [SerializeField] private Transform target = null;
        [SerializeField] private Vector3 offset = Vector3.one;
        [SerializeField] private float forwardLookOffset = 10f;
        [SerializeField] private float speed = 1f;
        [SerializeField] private bool lerpPosition = false;

        public void SetTarget(Transform target)
        {
            this.target = target;
            Vector3 realOffset = target.right * offset.x + target.up * offset.y + target.forward * offset.z;
            transform.position = target.position + realOffset;
        }

        private void Awake()
        {
            instance = this;
        }
        
        private void LateUpdate()
        {
            if (target == null)
                return;

            Vector3 realOffset = target.right * offset.x + target.up * offset.y + target.forward * offset.z;
            
            if (lerpPosition)
                transform.position = Vector3.MoveTowards (transform.position, target.position + realOffset, Time.deltaTime * speed);
            else
                transform.position = target.position + realOffset;
            
            transform.LookAt(target.position + target.forward * forwardLookOffset);
        }
    }
}
