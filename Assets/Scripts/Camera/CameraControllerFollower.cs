﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Racing.Camera
{
    public class CameraControllerFollower : MonoBehaviour
    {
        public static CameraControllerFollower instance;

        [SerializeField] private Transform target = null;
        
        [SerializeField] private Vector3 offset = Vector2.one;
        [SerializeField] private float horizontalRotSpeed = 10f;
        [SerializeField] private float moveSpeed = 1f;

        private Vector3 overridedOffset = Vector3.one;
        private float zRot;
        private bool isOffsetOverrided = false;
        
        private Transform aim = null;
        private UnityEngine.Camera cam = null;
        private float cameraFov = 60f;
        [SerializeField] private float fovShift = 1.2f;

        private bool isTargeted = false;
        
        private void Awake()
        {
            instance = this;

            if (target == null)
            {
                Debug.LogError("Can not find camera target!");
            }
            
            cam = UnityEngine.Camera.main;
            cameraFov = cam.fieldOfView;
        }
        
        public void SetTarget(Transform prnt)
        {
            aim = prnt;
            target.parent = null;
            target.position = aim.position;
            transform.position = target.position + target.up * 200f - target.forward * 200f;
            transform.LookAt(target);
            overridedOffset = offset;
            isTargeted = true;
        }
        
        public void Untarget()
        {
            isTargeted = false;
            isOffsetOverrided = false;
        }
        
        public void OverrideOffset(Vector3 offset, Vector3 angle)
        {
            isOffsetOverrided = true;
            overridedOffset = offset;
        }
        
        public void SetSpeedOffset(bool isOn)
        {
            cam.DOFieldOfView(isOn ? fovShift * cameraFov  : cameraFov, 0.5f);
        }
        
        private void LateUpdate()
        {
            if (!isTargeted)
                return;

            var currOffset = isOffsetOverrided ? overridedOffset : offset;
            target.position = aim.position + aim.up * currOffset.x;
            var pos = target.position + currOffset.y * aim.forward + currOffset.z * aim.up;
            transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * moveSpeed);
            var rot = Quaternion.LookRotation(target.position - transform.position, aim.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * horizontalRotSpeed);
        }
    }
}
