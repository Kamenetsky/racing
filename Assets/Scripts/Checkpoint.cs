﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Racing
{
    [System.Serializable]
    public class Checkpoint
    {
        public Vector3 position;
        public Vector3 normal;
        
        public static int GetCheckpointByOffset(List<Checkpoint> chpts, int id, int offset)
        {
            int count = chpts.Count;
            int result = id + offset;
            if (result >= count)
                result = id + offset - count;
            else if (result < 0)
                result = count + offset - id;
            
            return result;
        }

        public static int GetOffsetCycled(List<Checkpoint> chpts, int idTarget, int id)
        {
            int min = Mathf.Min(idTarget, id);
            int max = Mathf.Max(idTarget, id);
            int dist1 = min + chpts.Count - max;
            int dist2 = max - min;
            return Mathf.Min(dist1, dist2);
        }
    }
}
