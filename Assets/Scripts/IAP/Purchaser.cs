﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;
using Product = UnityEngine.Purchasing.Product;

namespace Racing.IAP
{
    public class Purchaser : MonoBehaviour, IStoreListener
	{
        public static Action<string> OnSuccess = delegate {};
        public static Action<string> OnError = delegate {};
        public static Action OnInitError = delegate { };
        public static Action <Product[]>OnInitSuccess = delegate { };

        public static Purchaser instance;

        private static IStoreController m_StoreController;
		private static IExtensionProvider m_StoreExtensionProvider;

        [HideInInspector]
        public Product[] products;

		void Start ()
		{
			if (m_StoreController == null) 
            {
				InitializePurchasing ();
			}

		}

		public void InitializePurchasing ()
		{
			if (IsInitialized ()) 
            {
				return;
			}

			var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());
#if UNITY_ANDROID || UNITY_EDITOR
            builder.AddProduct("crystal_pack_500", ProductType.Consumable, new IDs() { {
					"crystal_pack_500",
					GooglePlay.Name
				}
			});
            builder.AddProduct("crystal_pack_1200", ProductType.Consumable, new IDs() { {
					"crystal_pack_1200",
					GooglePlay.Name
				}
			});
#endif
            UnityPurchasing.Initialize (this, builder);
		}


		private bool IsInitialized ()
		{
			return m_StoreController != null && m_StoreExtensionProvider != null;
		}

        public Product GetProductInfo(string productId)
        {
            return m_StoreController.products.WithID(productId);
        }

		public void BuyProductID (string productId)

		{
			try 
            {
				if (IsInitialized ()) 
                {
					Product product = m_StoreController.products.WithID (productId);

					if (product != null && product.availableToPurchase) 
                    {
						Debug.Log (string.Format ("Purchasing product asychronously: '{0}'", product.definition.id));
						m_StoreController.InitiatePurchase (product);
					}
					else 
                    {
						Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
					}
				}
				else 
                {
					Debug.Log ("BuyProductID FAIL. Not initialized.");
				}
			}
			catch (Exception e) 
            {
				Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
			}
		}


		public void RestorePurchases ()
		{
			if (!IsInitialized ()) 
            {
				Debug.Log ("RestorePurchases FAIL. Not initialized.");
				return;
			}

			if (Application.platform == RuntimePlatform.IPhonePlayer ||
			    Application.platform == RuntimePlatform.OSXPlayer) 
            {
				Debug.Log ("RestorePurchases started ...");

				var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();
				apple.RestoreTransactions ((result) => 
                {
					Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				});
			}
			else 
            {
				Debug.Log ("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
			}
		}

		public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
		{
			Debug.Log ("OnInitialized: PASS");
			m_StoreController = controller;
			m_StoreExtensionProvider = extensions;
			Debug.Log ("Available items: " + controller.products.all.Length );
			foreach (var item in controller.products.all) 
            {
				if (item.availableToPurchase) 
                {
					Debug.Log (string.Join (" - ",
						new[] {
							item.metadata.localizedTitle,
							item.metadata.localizedDescription,
							item.metadata.isoCurrencyCode,
							item.metadata.localizedPrice.ToString (),
							item.metadata.localizedPriceString
						}));
				}
			}
            instance = this;
            products = controller.products.all;
            OnInitSuccess(controller.products.all);
		}


		public void OnInitializeFailed (InitializationFailureReason error)
		{
			Debug.Log ("OnInitializeFailed InitializationFailureReason:" + error);
            OnInitError();
		}


		public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
		{
            OnSuccess(args.purchasedProduct.definition.id);
            return PurchaseProcessingResult.Complete;
		}


		public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason)
		{
			Debug.Log (string.Format ("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
            OnError(product.definition.id);
		}
	}
}