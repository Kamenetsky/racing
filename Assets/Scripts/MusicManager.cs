﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Racing
{
    public class MusicManager : MonoBehaviour
    {
        public static MusicManager instance = null;
        
        [SerializeField] private List<AudioClip> clips = new List<AudioClip>();
        [SerializeField] private AudioSource source = null;
        [SerializeField] private AudioClip menuMusic = null;

        private AudioClip currentClip = null;

        private void Awake()
        {
            instance = this;
        }

        public void RefreshMenuMusic(bool play = true)
        {
            source.Stop();
            if (play)
            {
                source.volume = 0.5f;
                source.PlayOneShot(menuMusic);
            }
        }

        public void PlayRandomClip()
        {
            if (clips.Count == 0)
                return;

            source.volume = 1f;
            currentClip = clips[Random.Range(0, clips.Count)];
            source.PlayOneShot(currentClip);
        }

        public void Stop()
        {
            source.Stop();
        }
        
        public void Pause(bool pause)
        {
            if (pause)
            {
                source.Pause();
            }
            else
            {
                source.UnPause();
            }
        }

        public float GetClipLenght()
        {
            if (currentClip == null)
                return 0;

            return currentClip.length;
        }
    }
}
