﻿using UnityEngine;
using UnityEngine.UI;
using Product = UnityEngine.Purchasing.Product;

using Racing.Utils;
using Racing.IAP;

namespace Racing.UI
{
    public class IapUI : MonoBehaviour
    {
        [SerializeField]
        Button[] iapButtons;

        //[SerializeField]
        //ShopUI shop;

        bool isInited = false;

        void Awake()
        {
            Purchaser.OnInitError += OnInitError;
            Purchaser.OnInitSuccess += OnInitSuccess;
            Purchaser.OnSuccess += OnBuySuccess;

            foreach (var it in iapButtons)
            {
                var listener = it.gameObject.GetStateListener();
                listener.OnEnabled += Init;
                listener.OnDisabled += () => isInited = false;
            }
        }

        void OnDestroy()
        {
            Purchaser.OnInitError -= OnInitError;
            Purchaser.OnInitSuccess -= OnInitSuccess;
            Purchaser.OnSuccess -= OnBuySuccess;
        }

        void OnButtonEnabled()
        {
            Init();
        }

        void Init()
        {
            if (isInited)
                return;
            
            isInited = true;
            
            for (int i = 0; i < iapButtons.Length; ++i)
            {
                iapButtons[i].gameObject.GetComponentInChildren<Text>().text = "";
                iapButtons[i].interactable = false;
            }

            if (Purchaser.instance == null || Application.internetReachability == NetworkReachability.NotReachable)
                return;

            for (int i = 0; i < Purchaser.instance.products.Length; ++i)
            {
                if (i < iapButtons.Length)
                {
                    iapButtons[i].interactable = true;
                    iapButtons[i].gameObject.GetComponentInChildren<Text>().text = Purchaser.instance.products[i].metadata.isoCurrencyCode
                        + " " + Purchaser.instance.products[i].metadata.localizedPriceString;
                }
            }
        }

        void OnInitSuccess(Product[] products)
        {
            Init();
        }

        void OnInitError()
        {
            foreach (var it in iapButtons)
            {
                it.interactable = false;
            }
        }

        public void OnBuyProduct(string productId)
        {
            Purchaser.instance.BuyProductID(productId);
        }

        void OnBuySuccess(string productId)
        {
            int amount = 0;
            if (productId == "crystal_pack_500")
            {
                amount = 500;
            }
            else if (productId == "crystal_pack_1200")
            {
                amount = 1200;
            }
            int newCrystals = PlayerPrefs.GetInt("crystals", 0) + amount;
            PlayerPrefs.SetInt("crystals", newCrystals);
            //shop.Refresh();
        }
    }
}
