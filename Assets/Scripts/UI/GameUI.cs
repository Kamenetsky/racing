﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Racing.AdMob;
using Racing.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Racing.UI
{
    [Serializable]
    public class TrackInfo
    {
        public string name;
        public GameObject model;
        public int cost;
        public bool isRewarded;
    }
    
    [Serializable]
    public class CarInfo
    {
        public string name;
        public GameObject model;
        public int cost;
        public bool isRewarded;
    }
    
    public class GameUI : MonoBehaviour
    {
        public static GameUI instance = null;
        
        public static Action OnStartGame = delegate {};
        public static Action OnGoMainMenu = delegate {};
        public static Action OnNitro = delegate {};

        [Header("Common")]
        [SerializeField] private GameObject panelMainMenu = null;
        [SerializeField] private GameObject panelGarage = null;
        [SerializeField] private GameObject panelGame = null;
        [SerializeField] private GameObject panelSelectRace = null;
        [SerializeField] private GameObject panelPause = null;
        [SerializeField] private GameObject panelLose = null;
        [SerializeField] private GameObject panelWin = null;
        [SerializeField] private GameObject panelPolicy = null;
        [SerializeField] private LeaderBoardUI leaderboardLose = null;
        [SerializeField] private LeaderBoardUI leaderboardWin = null;

        [SerializeField] private Text timerText = null;
        [SerializeField] private Text lifesText = null;
        [SerializeField] private Text scoresText = null;

        [SerializeField] private List<GameObject> nitroObjects = new List<GameObject>();

        [SerializeField] private Image brokenGlass = null;
        [SerializeField] private Image fader = null;
        
        [Header("Cars")]
        [SerializeField] private GameObject carPreviewerRoot = null;
        [SerializeField] private Transform carPlace = null;
        [SerializeField] private TextMeshProUGUI carCaption = null;
        [SerializeField] private List<CarInfo> carInfos = new List<CarInfo>();
        [SerializeField] private GameObject selectCarBtn = null;
        [SerializeField] private GameObject buyCarBtn = null;
        private int currentCarIndex = 0;
        private int selectedCarIndex = 0;
        
        [Header("Tracks")]
        [SerializeField] private TextMeshProUGUI trackCaption = null;
        [SerializeField] private List<TrackInfo> trackInfos = new List<TrackInfo>();
        [SerializeField] private GameObject selectTrackBtn = null;
        [SerializeField] private GameObject buyTrackBtn = null;
        private int currentTrackIndex = 0;
        private int selectedTrackIndex = 0;

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            OnMainMenu();
        }

        public void UpdateNitro(int count)
        {
            for (int i = 0; i < nitroObjects.Count; ++i)
            {
                nitroObjects[i].SetActive(count == i);
            }
        }

        public void SetGameTime(float timer)
        {
            timerText.text = TimeToStringMMSS(timer);
        }

        public static string TimeToStringMMSS(float time)
        {
            string minutes = Mathf.Floor(time / 60).ToString("00");
            string seconds = (time % 60).ToString("00");
            return string.Format("{0}:{1}", minutes, seconds);
        }

        public void UpdateGameInfo(int lifes, int scores)
        {
            lifesText.text = "Lifes: " + lifes.ToString();
            scoresText.text = scores.ToString();
        }

        public void ShowBroken()
        {
            brokenGlass.gameObject.SetActive(true);
            brokenGlass.color = Color.white;
            brokenGlass.DOFade(0, 0.5f).OnComplete(() => brokenGlass.gameObject.SetActive(false));
        }

        public void OnStart()
        {
            selectedTrackIndex = currentTrackIndex;
            PlayerPrefs.SetInt("CurrentCarIndex", selectedCarIndex);
            PlayerPrefs.SetInt("CurrentLevelIndex", selectedTrackIndex);
            Time.timeScale = 1f;
            panelPause.SetActive(false);
            panelWin.SetActive(false);
            panelLose.SetActive(false);
            panelGarage.SetActive(false);
            panelSelectRace.SetActive(false);
            panelGame.SetActive(true);
            panelMainMenu.SetActive(false);
            carPreviewerRoot.SetActive(false);
            MusicManager.instance.RefreshMenuMusic(false);
            fader.DOFade(0f, 1f);
            OnStartGame();
            AdMobHelper.instance.ShowInterstitialAd();
        }

        public void OnMainMenu()
        {
            Time.timeScale = 1f;
            panelMainMenu.SetActive(true);
            panelGarage.SetActive(false);
            panelGame.SetActive(false);
            panelPause.SetActive(false);
            panelWin.SetActive(false);
            panelLose.SetActive(false);
            panelSelectRace.SetActive(false);
            carPreviewerRoot.SetActive(true);
            RefreshCar(selectedCarIndex);
            RefreshTrack(selectedTrackIndex);
            OnGoMainMenu();
            MusicManager.instance.RefreshMenuMusic();
            fader.DOFade(1f, 0.1f);
            currentCarIndex = PlayerPrefs.GetInt("CurrentCarIndex", 0);
            currentTrackIndex = PlayerPrefs.GetInt("CurrentLevelIndex", 0);

            if (PlayerPrefs.GetInt("GameComplete", 0) == 1)
            {
                PlayerPrefs.SetInt("GameComplete", 0);
                AdMobHelper.instance.ShowInterstitialAd();
            }
        }

        public void OnGarage()
        {
            panelMainMenu.SetActive(false);
            panelGarage.SetActive(true);
            RefreshCar(currentCarIndex);
        }

        public void OnSelectCar()
        {
            selectedCarIndex = currentCarIndex;
            RefreshCar(currentCarIndex);
        }

        public void OnBuyCar()
        {
            AdMobHelper.instance.ShowRewardedAd(RewardType.Car, currentCarIndex);
        }
        
        public void OnBuyMap()
        {
            AdMobHelper.instance.ShowRewardedAd(RewardType.Map, currentTrackIndex);
        }

        public void OnShowRewardComplete(RewardType rewardType, int rewardId)
        {
            if (rewardType == RewardType.Car)
            {
                PlayerPrefs.SetInt("IsCarBoth" + rewardId.ToString(), 1);
                RefreshCar(rewardId);
            }
            else if (rewardType == RewardType.Map)
            {
                PlayerPrefs.SetInt("IsMapBoth" + rewardId.ToString(), 1);
                RefreshTrack(rewardId);
            }
        }

        public void OnNextCar(bool right)
        {
            currentCarIndex += right ? 1 : -1;

            if (currentCarIndex >= carInfos.Count)
                currentCarIndex = 0;

            if (currentCarIndex < 0)
                currentCarIndex = carInfos.Count - 1;

            RefreshCar(currentCarIndex);
        }

        public void OnNextTrack(bool right)
        {
            currentTrackIndex += right ? 1 : -1;
            
            if (currentTrackIndex >= trackInfos.Count)
                currentTrackIndex = 0;

            if (currentTrackIndex < 0)
                currentTrackIndex = trackInfos.Count - 1;

            RefreshTrack(currentTrackIndex);
        }

        public void OnPause(bool pause)
        {
            Time.timeScale = pause ? 0 : 1;
            panelPause.SetActive(pause);
            MusicManager.instance.Pause(pause);
            if (pause)
            {
                AdMobHelper.instance.ShowInterstitialAd();
            }
        }

        public void OnSelectTrack()
        {
            panelMainMenu.SetActive(false);
            panelGarage.SetActive(false);
            panelSelectRace.SetActive(true);
            carPreviewerRoot.SetActive(false);
            RefreshTrack(currentTrackIndex);
        }

        public void ShowLose(float time)
        {
            leaderboardLose.Refresh(time, false);
            panelLose.SetActive(true);
        }
        
        public void ShowWin(float time)
        {
            leaderboardWin.Refresh(time, true);
            panelWin.SetActive(true);
        }

        public void OnNitroStart()
        {
            OnNitro();
        }

        public void OnExit()
        {
            PlayerPrefs.SetInt("GameComplete", 0);
            Application.Quit();
        }

        public void OnPrivacy(bool show)
        {
            panelPolicy.SetActive(show);
        }

        private void RefreshCar(int index)
        {
            bool isCarBouth = !carInfos[index].isRewarded || (PlayerPrefs.GetInt("IsCarBoth" + index, 0) == 1);
            bool isSelected = index == selectedCarIndex;
            selectCarBtn.SetActive(isCarBouth && !isSelected);
            buyCarBtn.SetActive(!isCarBouth);
            carCaption.text = carInfos[index].name;
            for (int i = 0; i < carInfos.Count; ++i)
            {
                carInfos[i].model.transform.position =
                    carPlace.position + (index == i ? Vector3.zero : Vector3.up * 1000);
            }
        }

        private void RefreshTrack(int index)
        {
            bool isTrackBouth = !trackInfos[index].isRewarded || (PlayerPrefs.GetInt("IsMapBoth" + index, 0) == 1);
            bool isSelected = index == selectedTrackIndex;
            selectTrackBtn.SetActive(isTrackBouth);
            buyTrackBtn.SetActive(!isTrackBouth);
            trackCaption.text = trackInfos[currentTrackIndex].name;
            for (int i = 0; i < trackInfos.Count; ++i)
            {
               trackInfos[i].model.SetActive(i == currentTrackIndex);
            }
        }
    }
}
