﻿
using System.Collections.Generic;
using Racing.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Racing.UI
{
    public class LeaderBoardUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI bestResTest = null;
        [SerializeField] private List<Text> texts = new List<Text>();

        public void Refresh(float result, bool isWin)
        {
            float bestResult = PlayerPrefs.GetFloat("BestResult", 0f);
            if (result > bestResult)
            {
                bestResult = result;
                PlayerPrefs.SetFloat("BestResult", bestResult);
            }

            bestResTest.text = "You best result: " + GameUI.TimeToStringMMSS(bestResult);
            
            System.Random rand = new System.Random((int)Time.time);
            RandomName nameGen = new RandomName(rand);
            List<string> names = nameGen.RandomNames(texts.Count);
            int playerPos = isWin ? 0 : Random.Range(1, 3);
            List<float> times = new List<float>();
            for (int i = 0; i < names.Count; ++i)
            {
                float fakeTime = (i < playerPos ? (result + Random.Range(2f, 35f)) : (result - Random.Range(3f, 30f)));
                if (fakeTime <= 0f)
                    fakeTime = Random.Range(10f, 15f);
                times.Add(fakeTime);
            }
            times.Sort((x, y) => y.CompareTo(x));
            for (int i = 0; i < texts.Count; ++i)
            {
                texts[i].color = Color.white;
                if (i == playerPos)
                {
                    texts[i].text = (i + 1) + ". YOU: " + GameUI.TimeToStringMMSS(result);
                    texts[i].color = Color.green;
                    continue;
                }
                texts[i].text = (i + 1) + ". " + names[i] + ": ";            
                texts[i].text += GameUI.TimeToStringMMSS(times[i]);
            }
        }
    }
}
