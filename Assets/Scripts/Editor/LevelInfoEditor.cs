﻿using Racing.Utils;
using UnityEditor;
using UnityEngine;

namespace Racing.Editor
{
    [CustomEditor(typeof(LevelInfo))]
    public class LevelInfoEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            if (GUILayout.Button("Analyze Road"))
            {
                LevelInfo info = (LevelInfo)target;
                AnalizeRoad(info);
            }

            if (GUILayout.Button("Interpolate Path"))
            {
                LevelInfo info = (LevelInfo)target;
                Interpolate(info);
            }
        }

        private void AnalizeRoad(LevelInfo info)
        {
            GetNearestRoadCheckpoints(info);
            
            if (info.LanesLeft.childCount == 0 || info.LanesRight.childCount == 0 || info.LanesCenter.childCount == 0)
            {
                Debug.LogError("Wrong checkpoints count!");
                return;
            }
            
            info.CheckpointsCenter.Clear();
            info.CheckpointsLeft.Clear();
            info.CheckpointsRight.Clear();
            
            info.CheckpointsCenterSmooth.Clear();
            info.CheckpointsLeftSmooth.Clear();
            info.CheckpointsRightSmooth.Clear();
            
            Debug.Log("left " + info.LanesLeft.childCount);
            Debug.Log("right " + info.LanesRight.childCount);
            Debug.Log("center " + info.LanesCenter.childCount);

            int leftIndex = 0;
            int rightIndex = 0;
            
            for (int i = 0; i < info.LanesCenter.childCount; ++i)
            {
                Checkpoint point = new Checkpoint();
                point.position = info.LanesCenter.GetChild(i).position;
                info.CheckpointsCenter.Add(point);

                if (i == 0)
                {
                    float distMax = float.MaxValue;
                    for (int j = 0; j < info.LanesLeft.childCount; ++j)
                    {
                        float dist = Vector3.Distance(info.LanesLeft.GetChild(j).position, point.position);
                        if (dist < distMax)
                        {
                            distMax = dist;
                            leftIndex = j;
                        }
                    }
                    
                    distMax = float.MaxValue;
                    for (int j = 0; j < info.LanesRight.childCount; ++j)
                    {
                        float dist = Vector3.Distance(info.LanesRight.GetChild(j).position, point.position);
                        if (dist < distMax)
                        {
                            distMax = dist;
                            rightIndex = j;
                        }
                    }
                }
                else
                {
                    leftIndex++;
                    if (leftIndex >= info.LanesLeft.childCount)
                        leftIndex = 0;

                    rightIndex++;
                    if (rightIndex >= info.LanesRight.childCount)
                        rightIndex = 0;
                }
                
                Checkpoint leftN = new Checkpoint();
                leftN.position = info.LanesLeft.GetChild(leftIndex).position;
                info.CheckpointsLeft.Add(leftN);
                
                Checkpoint rightN = new Checkpoint();
                rightN.position = info.LanesRight.GetChild(rightIndex).position;
                info.CheckpointsRight.Add(rightN);

                if (i > 0)
                {
                    point.normal = -Vector3.Cross(point.position - rightN.position,
                        info.CheckpointsCenter[i - 1].position - point.position).normalized;

                    if (Vector3.Dot(point.normal, Vector3.up) < 0f)
                        point.normal = -point.normal;
                    
                    leftN.normal = -Vector3.Cross(leftN.position - point.position,
                        info.CheckpointsLeft[i - 1].position - info.CheckpointsLeft[i].position).normalized;
                    if (Vector3.Dot(leftN.normal, Vector3.up) < 0f)
                        leftN.normal = -leftN.normal;
                    
                    rightN.normal = -Vector3.Cross(point.position - rightN.position,
                        info.CheckpointsRight[i - 1].position - info.CheckpointsRight[i].position).normalized;
                    if (Vector3.Dot(rightN.normal, Vector3.up) < 0f)
                        rightN.normal = -rightN.normal;
                }
                
                EditorUtility.DisplayProgressBar("Progress", "", (float)i / (float)info.LanesCenter.childCount);
            }

            if (info.isCycled)
            {
                info.CheckpointsCenter[0].normal = -Vector3.Cross(info.CheckpointsCenter[info.CheckpointsCenter.Count - 1].position - 
                                                                                                info.CheckpointsRight[info.CheckpointsCenter.Count - 1].position,
                    info.CheckpointsCenter[info.CheckpointsCenter.Count - 2].position - 
                    info.CheckpointsCenter[info.CheckpointsCenter.Count - 1].position).normalized;
                if (Vector3.Dot(info.CheckpointsCenter[0].normal, Vector3.up) < 0f)
                    info.CheckpointsCenter[0].normal = -info.CheckpointsCenter[0].normal;
                
                info.CheckpointsLeft[0].normal = -Vector3.Cross(info.CheckpointsLeft[info.CheckpointsLeft.Count - 1].position - 
                                                                 info.CheckpointsCenter[info.CheckpointsCenter.Count - 1].position,
                    info.CheckpointsLeft[info.CheckpointsLeft.Count - 2].position - 
                    info.CheckpointsLeft[info.CheckpointsLeft.Count - 1].position).normalized;
                if (Vector3.Dot(info.CheckpointsLeft[0].normal, Vector3.up) < 0f)
                    info.CheckpointsLeft[0].normal = -info.CheckpointsLeft[0].normal;
                
                info.CheckpointsRight[0].normal = -Vector3.Cross(info.CheckpointsRight[info.CheckpointsRight.Count - 1].position - 
                                                               info.CheckpointsCenter[info.CheckpointsCenter.Count - 1].position,
                    info.CheckpointsRight[info.CheckpointsRight.Count - 2].position - 
                    info.CheckpointsRight[info.CheckpointsRight.Count - 1].position).normalized;
                if (Vector3.Dot(info.CheckpointsRight[0].normal, Vector3.up) < 0f)
                    info.CheckpointsRight[0].normal = -info.CheckpointsRight[0].normal;
            }

            Interpolate(info);
            EditorUtility.SetDirty(info);
            
            EditorUtility.ClearProgressBar();
        }

        private void Interpolate(LevelInfo info)
        {
            if (info.CheckpointsLeft.Count == 0)
            {
                Debug.Log("Can not find checkpoints!");
                return;
            }
            
            info.CheckpointsCenterSmooth.Clear();
            info.CheckpointsLeftSmooth.Clear();
            info.CheckpointsRightSmooth.Clear();
            
            int amount = info.CheckpointsCenter.Count * info.InterpSteps;
            for (int i = 0; i < amount; i++)
            {
                info.CheckpointsCenterSmooth.Add(Bezier.GetCheckpoint(info.CheckpointsCenter, (float)i / amount ));
                info.CheckpointsLeftSmooth.Add(Bezier.GetCheckpoint(info.CheckpointsLeft, (float)i / amount ));
                info.CheckpointsRightSmooth.Add(Bezier.GetCheckpoint(info.CheckpointsRight, (float)i / amount ));
            }
        }

        private void GetNearestRoadCheckpoints(LevelInfo info)
        {
            if (info.MainRoad == null || info.MainRoad.Count == 0)
                return;
            
            info.NearestRoadCheckpoints.Clear();
            for (int i = 0; i < info.MainRoad.Count; ++i)
            {
                var pos = info.MainRoad[i].GetComponent<MeshRenderer>().bounds.center;
                float dist = float.MaxValue;
                int index = 0;
                for (int j = 0; j < info.CheckpointsCenter.Count; ++j)
                {
                    float currDist = Vector3.Distance(pos, info.CheckpointsCenter[j].position);
                    if (currDist < dist)
                    {
                        dist = currDist;
                        index = j;
                    }
                }
                info.NearestRoadCheckpoints.Add(index);
            }
        }
        
        public static bool IsCBetweenAB (Vector3 A, Vector3 B, Vector3 C)
        {
            return Vector3.Dot((B - A).normalized , (C - B).normalized) < 0f && Vector3.Dot((A - B).normalized, (C - A).normalized) < 0f;
        }
    }
}
