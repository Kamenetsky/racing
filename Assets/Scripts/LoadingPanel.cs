﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Racing
{
    public class LoadingPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI loadingText = null;

        private int currDots = 0;
        private float time = 0;
        private const float dTime = 0.3f;

        private void Start()
        {
            StartCoroutine(LoadAsync());
        }

        private IEnumerator LoadAsync()
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Game");

            while (!asyncLoad.isDone)
            {
                time += Time.deltaTime;
                if (time > dTime)
                {
                    time = 0f;
                    currDots++;
                    if (currDots > 3)
                        currDots = 0;
                }
                string str = String.Empty;
                for (int i = 0; i < currDots; ++i)
                    str += ".";
                
                loadingText.text = "loading" + str;
                yield return null;
            }
        }
    }
}
