﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Racing
{
    public class TouchController : MonoBehaviour
    {
        public static Action<bool> OnTouch = delegate { };
        
        void Update () 
        {
            if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.currentSelectedGameObject != null)
            {
                return;
            }
            
            if (Input.GetMouseButtonDown(0)) 
            {
                if (Input.mousePosition.x > Screen.width / 2)
                {
                    OnTouch(true);
                }
                
                if (Input.mousePosition.x < Screen.width / 2)
                {
                    OnTouch(false);
                }
            }
        }
    }
}
