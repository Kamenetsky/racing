﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Racing.Utils;
using UnityEngine;
using Random = System.Random;

namespace Racing
{
    
    [Serializable]
    public class DecorInfo
    {
        public GameObject decorPrefab = null;
        public bool isTimed = false;
        public float spawnInterval = 3f;
        public float spawnTime = 0f;

        public int spawnIndexMinInterval = 70;
        public int spawnIndexMaxInterval = 100;
        public int spawnIndexConstInterval = 50;
        
        public int spawnCurrIndex = 0;
        

        public bool isOriented = true;
        public Vector3 addRotation = Vector3.zero;
        public float centerOffset = 0f;
        public bool randomOffset = false;
        public float destroyTime = 20f;

        public bool isColorChanges = false;
    }
    
    public class LevelInfo : MonoBehaviour
    {
        [SerializeField] private Transform lanesLeft = null;
        [SerializeField] private Transform lanesCenter = null;
        [SerializeField] private Transform lanesRight = null;
        
        [SerializeField] private List<DecorInfo> decors = new List<DecorInfo>();
        [SerializeField] private List<GameObject> mainRoad = null;
        [SerializeField] private int roadChangeOffsetId = 300;

        private int currentRoadPartId = 0;

        public bool isCycled = true;
        
        public Transform LanesLeft => lanesLeft;
        public Transform LanesCenter => lanesCenter;
        public Transform LanesRight => lanesRight;
        
        public List<Checkpoint> CheckpointsLeft => checkpointsLeft;
        public List<Checkpoint> CheckpointsCenter => checkpointsCenter;
        public List<Checkpoint> CheckpointsRight => checkpointsRight;
        
        public List<Checkpoint> CheckpointsLeftSmooth => checkpointsLeftSmooth;
        public List<Checkpoint> CheckpointsCenterSmooth => checkpointsCenterSmooth;
        public List<Checkpoint> CheckpointsRightSmooth => checkpointsRightSmooth;

        public List<GameObject> MainRoad => mainRoad;
        public List<int> NearestRoadCheckpoints => nearestRoadCheckpoints;

        [SerializeField] private bool isDebug = false;

        private List<Checkpoint> lane = null;

        private RaceColorChangerController colorChangerController = null;
        
        [HideInInspector][SerializeField] private List<Checkpoint> checkpointsLeft = new List<Checkpoint>();
        [HideInInspector][SerializeField] private List<Checkpoint> checkpointsCenter = new List<Checkpoint>();
        [HideInInspector][SerializeField] private List<Checkpoint> checkpointsRight = new List<Checkpoint>();
        
        [Range(2, 100)]
        [SerializeField] private int interpSteps = 4;

        public int InterpSteps => interpSteps;
        
        [HideInInspector][SerializeField] private List<Checkpoint> checkpointsLeftSmooth = new List<Checkpoint>();
        [HideInInspector][SerializeField] private List<Checkpoint> checkpointsCenterSmooth = new List<Checkpoint>();
        [HideInInspector][SerializeField] private List<Checkpoint> checkpointsRightSmooth = new List<Checkpoint>();
        
        [HideInInspector][SerializeField] private List<int> nearestRoadCheckpoints = new List<int>();

        private Transform cam = null;
        private List<GameObject> decorObjects = new List<GameObject>();

        public void SetLane(List<Checkpoint> lane)
        {
            this.lane = lane;
        }

        public void CheckDecorSpawn(int checkpoindIndex)
        {
            UpdateRace(checkpoindIndex);
            
            foreach (var it in decors)
            {
                if (!it.isTimed)
                {
                    it.spawnCurrIndex++;
                    if (it.spawnCurrIndex > it.spawnIndexConstInterval)
                    {
                        it.spawnCurrIndex = 0;
                        SpawnDecor(it, checkpoindIndex);
                    }
                }
                else if (it.spawnTime > it.spawnInterval)
                {
                    it.spawnTime = 0f;
                    SpawnDecor(it, checkpoindIndex);
                }
            }
        }

        private void SpawnDecor(DecorInfo info, int checkpoindIndex)
        {
            var decor = Instantiate(info.decorPrefab);
            int checkpointID = checkpoindIndex + (info.isTimed ? UnityEngine.Random.Range(info.spawnIndexMinInterval, info.spawnIndexMaxInterval)
                                   : info.spawnIndexConstInterval);

            if (checkpointID >= lane.Count)
            {
                checkpointID = info.spawnIndexMaxInterval;
            }
            decor.transform.position = lane[checkpointID].position;
            if (info.isOriented)
            {
                decor.transform.LookAt(lane[checkpointID > 0 ? checkpointID - 1 : checkpointID + 1].position);
            }
            decor.transform.rotation *= Quaternion.Euler(info.addRotation);
            int koef = 1;
            if (info.randomOffset)
            {
                koef = UnityEngine.Random.value > 0.5f ? -1 : 1;
            }
            decor.transform.position += decor.transform.forward * info.centerOffset * koef;
            decor.transform.parent = transform;

            if (colorChangerController != null && info.isColorChanges)
            {
                colorChangerController.AddGameObject(decor);
            }
            decorObjects.Add(decor);
            decor.GetStateListener().OnDestroied += delegate { decorObjects.Remove(decor); };
            Destroy(decor, info.destroyTime);
            
            for (int i = 0; i < decorObjects.Count; ++i)
            {
                var heading = decorObjects[i].transform.position - cam.transform.position;
                var dot = Vector3.Dot(heading, cam.transform.forward);

                if (dot < 0f)
                {
                    Destroy(decorObjects[i]);
                }
            }
        }

        private void UpdateRace(int checkpointIndex)
        {
            if (mainRoad == null || mainRoad.Count <= 1)
                return;

            int nextRoadPardId = currentRoadPartId == mainRoad.Count - 1 ? 0 : currentRoadPartId + 1;
            if (Checkpoint.GetOffsetCycled(checkpointsCenter, nearestRoadCheckpoints[currentRoadPartId],
                    checkpointIndex) + roadChangeOffsetId >
                Checkpoint.GetOffsetCycled(checkpointsCenter, nearestRoadCheckpoints[nextRoadPardId], checkpointIndex))
            {
                currentRoadPartId++;
                if (currentRoadPartId == mainRoad.Count)
                {
                    currentRoadPartId = 0;
                }

                ActivateRaceParts();
            }
        }

        private void ActivateRaceParts()
        {
            int nextRoadPardId = currentRoadPartId == mainRoad.Count - 1 ? 0 : currentRoadPartId + 1;
            for (int i = 0; i < mainRoad.Count; ++i)
            {
                mainRoad[i].SetActive(i == currentRoadPartId || i == nextRoadPardId);
            }
        }

        private void Awake()
        {
            colorChangerController = GetComponentInChildren<RaceColorChangerController>();
            cam = UnityEngine.Camera.main.transform;
        }

        private void Start()
        {
            if (colorChangerController != null && mainRoad != null && mainRoad.Count > 0)
            {
                foreach (var it in mainRoad)
                {
                    colorChangerController.AddGameObject(it);
                }
            }
            
            foreach (var it in decors)
            {
                if (it.isTimed)
                    continue;

                SpawnDecor(it, 0);
            }

            ActivateRaceParts();
        }

        private void Update()
        {
            foreach (var it in decors)
            {
                if (!it.isTimed)
                    continue;

                it.spawnTime += Time.deltaTime;
            }
        }

        private void OnDrawGizmos()
        {
            if (!isDebug)
                return;
            
            int num = 0;
            
            Gizmos.color = Color.red;
            foreach (var it in CheckpointsCenter)
            {
                ++num;
                if (num > 10)
                    break;
                
                Gizmos.DrawSphere(it.position, 1);

                if (it.normal != null)
                {
                    //Gizmos.DrawCube(it.position + it.normal * 5, Vector3.one);
                    Gizmos.DrawRay(it.position, it.normal * 5);
                }
            }

            num = 0;
            
            Gizmos.color = Color.yellow;
            foreach (var it in CheckpointsLeft)
            {
                ++num;
                if (num > 10)
                    break;
                
                Gizmos.DrawSphere(it.position, 1);

                if (it.normal != null)
                {
                    //Gizmos.DrawCube(it.position + it.normal * 5, Vector3.one);
                    Gizmos.DrawRay(it.position, it.normal * 5);
                }
            }
            
            num = 0;
            
            Gizmos.color = Color.green;
            foreach (var it in CheckpointsRight)
            {
                ++num;
                if (num > 10)
                    break;
                
                Gizmos.DrawSphere(it.position, 1);

                if (it.normal != null)
                {
                    //Gizmos.DrawCube(it.position + it.normal * 5, Vector3.one);
                    Gizmos.DrawRay(it.position, it.normal * 5);
                }
            }

            Gizmos.color = Color.cyan;
            for (int i = 0; i < Mathf.Min(300, CheckpointsCenterSmooth.Count); ++i)
            {
                Gizmos.DrawCube(CheckpointsCenterSmooth[i].position, Vector3.one * 0.3f);
                Gizmos.DrawRay(CheckpointsCenterSmooth[i].position, CheckpointsCenterSmooth[i].normal * 3);
                
                Gizmos.DrawCube(CheckpointsLeftSmooth[i].position, Vector3.one * 0.3f);
                Gizmos.DrawRay(CheckpointsLeftSmooth[i].position, CheckpointsLeftSmooth[i].normal * 3);
            }
        }
    }
}
