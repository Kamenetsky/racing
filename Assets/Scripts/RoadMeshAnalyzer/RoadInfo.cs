﻿using System.Collections.Generic;
using UnityEngine;

namespace Racing.RoadMeshAnalyzerEditor
{
    [System.Serializable]
    public class LaneInfo
    {
        public List<Vector3> checkpoints = new List<Vector3>();
    }
    
    [CreateAssetMenu(fileName = "RoadInfo", menuName = "Roads/RoadInfo", order = 1)]
    public class RoadInfo : ScriptableObject
    {
        public int lanesCount = 2;
        public float laneWidth;
        public float interval;
        
        public List<LaneInfo> laneInfos = new List<LaneInfo>();

        public GameObject road;
    }
}
