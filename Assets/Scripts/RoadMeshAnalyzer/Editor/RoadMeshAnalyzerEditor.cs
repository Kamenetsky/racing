﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Racing.RoadMeshAnalyzerEditor.Editor
{
    [CustomEditor(typeof(RoadInfo))]
    public class RoadMeshAnalyzerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            if (GUILayout.Button("Analyze Mesh"))
            {
                RoadInfo info = (RoadInfo)target;
                AnalyzeMesh(info);
            }
        }
        
#if UNITY_EDITOR  
        private static void AnalyzeMesh(RoadInfo info)
        {
            //if (Selection.gameObjects.Length == 0)
                //return;

            //var meshFilter = Selection.gameObjects[0].GetComponent<MeshFilter>();

            if (info.road == null)
                return;
            
            var meshFilter = info.road.GetComponentInChildren<MeshFilter>();

            if (meshFilter == null)
            {
                Debug.LogWarning("Wrong path prefab!");
                return;
            }

            if (meshFilter.sharedMesh.vertices.Length < 10)
            {
                Debug.LogWarning("To short path!");
                return;
            }
            
            info.interval = Vector3.Distance(meshFilter.transform.TransformPoint(meshFilter.sharedMesh.vertices[0]),
                            meshFilter.transform.TransformPoint(meshFilter.sharedMesh.vertices[1]));
            
            info.laneWidth = Vector3.Distance(meshFilter.transform.TransformPoint(meshFilter.sharedMesh.vertices[1]),
                meshFilter.transform.TransformPoint(meshFilter.sharedMesh.vertices[2]));
            int laneIndex = 0;
            info.laneInfos = new List<LaneInfo>(info.lanesCount);
            for (int i = 0; i < info.lanesCount; ++i)
            {
                var li = new LaneInfo();
                li.checkpoints = new List<Vector3>();
                info.laneInfos.Add(li);
            }
            
            for (int i = 0; i < meshFilter.sharedMesh.vertices.Length; ++i)
            {
                Vector3 pos0 = meshFilter.transform.TransformPoint(meshFilter.sharedMesh.vertices[i]);
                if (i < meshFilter.sharedMesh.vertices.Length - 1)
                {
                    Vector3 pos1 = meshFilter.transform.TransformPoint(meshFilter.sharedMesh.vertices[i + 1]);
                    if (Vector3.Distance(pos0, pos1) < Mathf.Min(info.interval, info.laneWidth) * 0.5f)
                    {
                        Debug.Log("Complete!");
                        break;
                    }
                }

                info.laneInfos[laneIndex].checkpoints.Add(pos0);
                ++laneIndex;
                if (laneIndex >= info.lanesCount)
                    laneIndex = 0;
            }
            
            //Check
            var helper = new GameObject();
            helper.transform.position = Vector3.zero;
            /*for (int i = 0; i < 100; ++i)
            {
                var pos = info.laneInfos[0].checkpoints[i];
                var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
                go.transform.SetParent(helper.transform);
                go.transform.position = pos;
            }*/
            
            for (int i = 0; i < 500; ++i)
            {
                var pos = meshFilter.transform.TransformPoint(meshFilter.sharedMesh.vertices[i]);;
                var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                go.transform.SetParent(helper.transform);
                go.transform.position = pos;
            }
        }
#endif
    }
}
