﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using DG.Tweening;

namespace Racing
{
    public enum ImpactType
    {
        Bonus = 0,
        Obstacle = 10,
        Heart = 20,
        Speed = 30,
    }
    
    public class CarView : MonoBehaviour
    {
        public static Action<ImpactType, GameObject> OnImpact = delegate { };
        
        [SerializeField] private Transform[] wheels = null;
        [SerializeField] private Transform body = null;

        [SerializeField] private ParticleSystem nitro = null;
        [SerializeField] private ParticleSystem gas = null;

        [SerializeField] private bool overrideCameraOffset = false;
        [SerializeField] private Vector3 offset = Vector3.zero;
        [SerializeField] private Vector3 angle = Vector3.zero;

        public bool OverrideCameraOffset => overrideCameraOffset;
        public Vector3 CameraOffset => offset;
        public Vector3 CameraAngle => angle;
        
        private float rotationSpeed = 0f;
        private List<Material> mats= new List<Material>();

        public Transform GetBody()
        {
            return body;
        }
        
        public void SetRotationSpeed(float speed)
        {
            rotationSpeed = speed;
        }

        public void TurnNitro(bool isOn)
        {
            if (isOn)
            {
                nitro.Play();
                gas.Stop();
            }
            else
            {
                gas.Play();
                nitro.Stop();
            }
        }

        public void DamageEffect()
        {
            foreach (var mat in mats)
            {
                if (mat.HasProperty("_Color"))
                    mat.DOFade(0f, 0.1f).OnComplete(() => mat.DOFade(1f, 0.1f)).SetLoops(5);
            }
        }

        private void Awake()
        {
            var rends = GetComponentsInChildren<MeshRenderer>();
            foreach (var rend in rends)
            {
                mats.AddRange(rend.materials);
            }
        }

        private void Start()
        {
            TurnNitro(false);
        }

        private void Update()
        {
            foreach (var wheel in wheels)
            {
                wheel.Rotate(Vector3.right, Time.deltaTime * rotationSpeed);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("bonus"))
            {
                SoundManager.instance.Play(SoundType.Bonus1);
                OnImpact(ImpactType.Bonus, other.gameObject);
            }
            else if (other.CompareTag("obstacle"))
            {
                SoundManager.instance.Play(SoundType.Impact);
                OnImpact(ImpactType.Obstacle, other.gameObject);
                DamageEffect();
            }
            else if (other.CompareTag("speed"))
            {
                SoundManager.instance.Play(SoundType.Bonus2);
                OnImpact(ImpactType.Speed, other.gameObject);
            }
            else if (other.CompareTag("heart"))
            {
                SoundManager.instance.Play(SoundType.Bonus2);
                OnImpact(ImpactType.Heart, other.gameObject);
            }
        }
    }
}
