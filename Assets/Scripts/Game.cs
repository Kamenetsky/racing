﻿using System.Collections.Generic;
using DG.Tweening;
using Racing.Camera;
using Racing.UI;
using Racing.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Racing
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private Transform[] carMovers = null;

        [SerializeField] private CarView[] carViewPrefabs = null;
        [SerializeField] private LevelInfo[] levelPrefabs = null;
        
        [SerializeField] private RoadObjectsManager roadObjectsManager = null;
        
        private LevelInfo currentLevel = null;
        private CarView carView = null;
        private List<Checkpoint> currentLane = null;
        private List<List<Checkpoint>> lanes = new List<List<Checkpoint>>();
        private List<List<Checkpoint>> checkpoints = new List<List<Checkpoint>>();
        private int currCheckpoint = 0;
        private int currCheckpointGlobal = 0;
        private int currLaneIndex = 1;
        
        private Dictionary<Checkpoint, GameObject> pointsByObjects = new Dictionary<Checkpoint, GameObject>();

        private float carSpeed = 135f;
        private float carShiftTime = 0.25f;
        private float carRotSpeed = 15f;
        private const float checkpointThreshold = 0.1f;
        private const float checkpointGlobalThreshold = 0.5f;

        private bool isTurning = false;

        private bool isNitro = false;
        private float nitroTime = 0f;
        private const float nitroUseTime = 5f;
        private const float nitroMultiplier = 2f;
        private int nitroScores = 0;
        private const int scoresToNitro = 5;

        private const float impactDampfer = 0.4f;
        private const float impactRecoverySpeed = 0.1f;
        public float impactCurrentMultiplier = 1f;
        
        private const int initLifes = 3;
        private int lifes = initLifes;

        private int scores = 0;

        private bool isStarted = false;

        private const float gameOverStartTime = 60f;
        private const float secondsPerBonus = 1.5f;
        private float gameCompleteTime = 0f;
        private float totalTime = 0f;
        private float gameTime = 0f;

        private void Awake()
        {
            TouchController.OnTouch += GoToNextLane;
            CarView.OnImpact += OnImpact;
            GameUI.OnNitro += OnNitro;
            GameUI.OnStartGame += OnRestart;
            GameUI.OnGoMainMenu += OnStop;
        }

        private void OnDestroy()
        {
            TouchController.OnTouch -= GoToNextLane;
            CarView.OnImpact -= OnImpact;
            GameUI.OnNitro -= OnNitro;
            GameUI.OnStartGame -= OnRestart;
            GameUI.OnGoMainMenu -= OnStop;
        }

        private void OnNitro()
        {
            if (isNitro)
                return;
            
            isNitro = true;
            carView.TurnNitro(true);
            nitroScores = 0;
            GameUI.instance.UpdateNitro(nitroScores);
            CameraControllerFollower.instance.SetSpeedOffset(true);
        }

        public void OnRestart()
        {
            OnStop();
            OnStart();
        }

        public void OnStop()
        {
            isStarted = false;
            lanes.Clear();
            checkpoints.Clear();
            CameraControllerFollower.instance.Untarget();
            if (carView != null)
                Destroy(carView.gameObject);
            carView = null;
            if (currentLevel != null)
                Destroy(currentLevel.gameObject);
            currentLevel = null;
            roadObjectsManager.UnInit();
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
            MusicManager.instance.Stop();
            for (int i = 2; i < 5; ++i)
            {
                if (SceneManager.GetSceneByBuildIndex(i).isLoaded)
                {
                    SceneManager.UnloadSceneAsync(SceneManager.GetSceneByBuildIndex(i));
                }    
            }
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (mode != LoadSceneMode.Additive)
                return;
            
            SceneManager.sceneLoaded -= OnSceneLoaded;
            CompleteStart();
        }

        public void OnStart()
        {
            int levelIndex = PlayerPrefs.GetInt("CurrentLevelIndex", 0);
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.LoadScene(levelIndex + 2, LoadSceneMode.Additive);
        }

        private void CompleteStart()
        {
            ShowGameParams();
            currCheckpoint = 0;
            currCheckpointGlobal = 1;
            currLaneIndex = 1;
            int levelIndex = PlayerPrefs.GetInt("CurrentLevelIndex", 0);
            int carIndex = PlayerPrefs.GetInt("CurrentCarIndex", 0);

            
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(levelIndex + 2));
            currentLevel = Instantiate(levelPrefabs[levelIndex]);   
            roadObjectsManager.Init(currentLevel);
           
            lanes.Clear();
            lanes.Add(currentLevel.CheckpointsLeftSmooth);
            lanes.Add(currentLevel.CheckpointsCenterSmooth);
            lanes.Add(currentLevel.CheckpointsRightSmooth);
            
            checkpoints.Clear();
            checkpoints.Add(currentLevel.CheckpointsLeft);
            checkpoints.Add(currentLevel.CheckpointsCenter);
            checkpoints.Add(currentLevel.CheckpointsRight);
            
            currentLevel.SetLane(checkpoints[1]);
            
            currentLane = currentLevel.CheckpointsCenterSmooth;
            
            for (int i = 0; i < carMovers.Length; ++i)
            {
                carMovers[i].position = lanes[i][0].position;
            }

            carView = Instantiate(carViewPrefabs[carIndex]);
            carView.transform.SetParent(carMovers[1]);
            carView.transform.localPosition = Vector3.zero;
            carView.transform.localRotation = Quaternion.identity;
            
            carView.SetRotationSpeed(2000f);
          
            CameraControllerFollower.instance.SetTarget(carView.transform);
            if (carView.OverrideCameraOffset)
            {
                CameraControllerFollower.instance.OverrideOffset(carView.CameraOffset, carView.CameraAngle);
            }

            lifes = initLifes;
            scores = 0;
            nitroScores = 0;
            GameUI.instance.UpdateNitro(0);
            MusicManager.instance.PlayRandomClip();
            gameCompleteTime = MusicManager.instance.GetClipLenght();
            totalTime = gameOverStartTime;
            gameTime = 0f;
            
            isStarted = true;
        }

        private void ShowGameParams()
        {
            GameUI.instance.UpdateGameInfo(lifes, scores);
            GameUI.instance.SetGameTime(totalTime);
        }
        
        private void Update()
        {
            if (!isStarted)
                return;

            if (isNitro)
            {
                nitroTime += Time.deltaTime;

                if (nitroTime > nitroUseTime)
                {
                    isNitro = false;
                    nitroTime = 0f;
                    carView.TurnNitro(false);
                    CameraControllerFollower.instance.SetSpeedOffset(false);
                }
            }

            gameCompleteTime -= Time.deltaTime;
            totalTime -= Time.deltaTime;
            gameTime += Time.deltaTime;

            ShowGameParams();

            if (gameCompleteTime <= 0)
            {
                GameComplete(true);
                return;
            }

            if (totalTime <= 0)
            {
                GameComplete(false);
                return;
            }
            
            float step = Time.deltaTime * (carSpeed * (isNitro ? nitroMultiplier : 1f));
            if (Vector3.Distance(carMovers[currLaneIndex].position, currentLane[currCheckpoint].position) <= checkpointThreshold)
            {
                currCheckpoint++; 
                
                if (currCheckpoint >= currentLane.Count) //TODO make for not cycled
                {
                    currCheckpoint = 0;
                }
            }

            
            if (Vector3.Distance(carMovers[currLaneIndex].position, checkpoints[currLaneIndex][currCheckpointGlobal].position) <=
                checkpointGlobalThreshold)
            {
                currCheckpointGlobal++;
                
                currentLevel.CheckDecorSpawn(currCheckpointGlobal);
                if (currCheckpointGlobal >= checkpoints[currLaneIndex].Count)
                    currCheckpointGlobal = 0;
            }

            if (impactCurrentMultiplier < 1f)
            {
                impactCurrentMultiplier += impactRecoverySpeed * Time.deltaTime;
            }

            for (int i = 0; i < carMovers.Length; ++i)
            {
                Vector3 nextPos = lanes[i][currCheckpoint].position;    
                var targetRotation = Quaternion.FromToRotation(Vector3.up, lanes[i][currCheckpoint].normal);
                var horizontalVector = new Vector3(nextPos.x - carMovers[i].position.x, 0f, nextPos.z - carMovers[i].position.z).normalized;
                if (horizontalVector != Vector3.zero)
                    targetRotation *= Quaternion.LookRotation(horizontalVector);
                carMovers[i].rotation = Quaternion.Slerp(carMovers[i].rotation, targetRotation, carRotSpeed * Time.deltaTime);
                carMovers[i].position = Vector3.MoveTowards(carMovers[i].position, nextPos, step);
            }
            roadObjectsManager.UpdateInfo(currCheckpointGlobal);
        }
        
        private void GoToNextLane(bool left)
        {
            if (!isStarted || isTurning)
                return;

            if (currLaneIndex == 0 && left)
                return;

            if (currLaneIndex >= 2 && !left)
                return;

            isTurning = true;

            currLaneIndex += left ? -1 : 1;

            GetNearestCheckpoints();
            
            carMovers[currLaneIndex].position = currentLane[currCheckpoint].position;
            carView.transform.SetParent(carMovers[currLaneIndex]);
            carView.transform.DOLocalMove(Vector3.zero, carShiftTime);
            carView.transform.DOLocalRotate(Vector3.zero, carShiftTime).OnComplete(() => isTurning = false);
            Vector3 rot = carView.transform.localEulerAngles + Vector3.up * (left ? 7f : -7f);
            carView.GetBody().DOLocalRotate(rot, carShiftTime * 0.5f).OnComplete(() =>
            {
                carView.GetBody().DOLocalRotate(Vector3.zero, carShiftTime * 0.5f); 
            });
        }

        private void GetNearestCheckpoints()
        {
            var prevLane = currentLane;
            currentLane = lanes[currLaneIndex];
            
            float distMax = float.MaxValue;
            int nearestIndex = currCheckpoint;
            for (int j = -currentLevel.InterpSteps; j < currentLevel.InterpSteps + 1; ++j)
            {
                int ind = Checkpoint.GetCheckpointByOffset(currentLane, currCheckpoint, j);
                float dist = Vector3.Distance(prevLane[currCheckpoint].position, currentLane[ind].position);
                if (dist < distMax)
                {
                    distMax = dist;
                    nearestIndex = ind;
                }
            }
            currCheckpoint = nearestIndex;

            //HACK
            currCheckpointGlobal += 2;
            if (currCheckpointGlobal >= checkpoints[currLaneIndex].Count)
                currCheckpointGlobal = 2;
        }

        private void OnImpact(ImpactType impactType, GameObject bonus)
        {
            if (impactType == ImpactType.Bonus)
            {
                var effect = bonus.GetComponentInChildren<ParticleSystem>();
                bonus.GetComponent<MeshRenderer>().enabled = false;
                effect.Play();
                scores += 1;
                if (nitroScores < scoresToNitro - 1)
                {
                    nitroScores += 1;
                    GameUI.instance.UpdateNitro(nitroScores);
                }

                totalTime += secondsPerBonus;
            }
            else if (impactType == ImpactType.Obstacle)
            {
                lifes--;
                impactCurrentMultiplier = impactDampfer;
                GameUI.instance.ShowBroken();
                if (lifes <= 0)
                {
                    GameComplete(false);
                }
            }
            else if (impactType == ImpactType.Speed)
            {
                isNitro = true;
                carView.TurnNitro(true);
                CameraControllerFollower.instance.SetSpeedOffset(true);
            }
            else if (impactType == ImpactType.Heart)
            {
                bonus.GetComponent<MeshRenderer>().enabled = false;
                lifes++;
            }
        }

        private void GameComplete(bool win)
        {
            isStarted = false;
            MusicManager.instance.Stop();
            
            PlayerPrefs.SetInt("GameComplete", 1);
            if (win)
            {
                GameUI.instance.ShowWin(gameTime);
            }
            else
            {
                GameUI.instance.ShowLose(gameTime);
            }
        }
    }
}
